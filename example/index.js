
const { getKairosTime, 
		convertKairoTimeArrToGregorian 
	} = require("@kairos-alliance/time-converter")


function handleKeyPress(event) {
    const key = event.keyCode || event.which;
    if (key == 13) {
        parseDate(event);
    }
}

function parseDate(event) {
    const input = document.getElementById("date").value
    const { day, 
            dayOfYear,
            teHalf,
            teHalfYr, 
            te, 
            teYear } = getKairosTime(input)

    document.getElementById("kt").innerHTML = `
        ${te}${teHalf}${teHalfYr}${dayOfYear}<br>
        [${te}${teHalf}, ${teHalfYr}, ${dayOfYear}]<br><br>
        (legacy) <br>
        ${te}${teYear}${dayOfYear} <br>
        [${te}, ${teYear}, ${dayOfYear}] 
    `
    document.getElementById("gregorian").innerHTML = `${ new Date(day)}`

}

function parseTimestamp(event) {
    const input = document.getElementById("timestamp").value
    console.log("input", input)
    const time = convertKairoTimeArrToGregorian(input)
    console.log(time)
    document.getElementById("gregorian-from-kt").innerHTML = time.full
}

window.handleKeyPress = handleKeyPress
window.parseDate = parseDate
window.parseTimestamp = parseTimestamp


