const { DateTime, toMillis } = require("luxon")

const firstYear = -4712    
const teLength = 38
const halfLength = 19

// TODO - BC numbers behave strangely
// TODO - GMT TIMEZONES
// UTC?

// 2022 is 177

function getTemporalExpanse(year) {
    const absoluteYear = -firstYear + year
    const te = Math.floor(absoluteYear / teLength)

    const teYear = absoluteYear % teLength

    // half te
    const teHalf = Math.floor(teYear / halfLength)
    const teHalfYr = teYear % halfLength

    return {
        teYear: teYear.toString().padStart(2, '0'),
        te: "".concat(te.toString().padStart(3, '0'),teHalf.toString()),
        teHalf: teHalf.toString(), // always one digit
        teHalfYr: teHalfYr.toString().padStart(2, '0'),

    }
}

function getKairosTime(input) {
    // BCE
    // https://github.com/moment/luxon/issues/774
    // if input has - then pad it
    const dateTime = DateTime.fromISO(input.toString(), { zone: "UTC" })
    dateTime.full = dateTime.toLocaleString(DateTime.DATE_MED_WITH_WEEKDAY)
    // day.weekday = day.toLocaleString(DateTime.DATE_MED_WITH_WEEKDAY)
    // console.log(day.toLocaleString(DateTime.DATE_FULL))
    const dayOfYear = dateTime.ordinal.toString().padStart(3, '0')
    const year = dateTime.year
    const {
        te,
        teHalf,
        teHalfYr,
        teYear
    } = getTemporalExpanse(year)
    // console.log(te, teYear, dayOfYear)
    return { te, teYear, teHalf, teHalfYr, dayOfYear, dateTime }
}

// TODO FIX
function convertKairoTimeArrToGregorian(inputArrOrString) {
    console.log(inputArrOrString)
    let inputArr
    if (inputArrOrString.constructor !== Array) {
        inputArr = [
            inputArrOrString.slice(0, 3),
            inputArrOrString.slice(3, 6),
            inputArrOrString.slice(6, 9)
        ]
    } else {
        inputArr = inputArrOrString[0]
    }

    if (!inputArr || !inputArr[0]) return

    const floorYear = (Number(inputArr[0]) * teLength) + firstYear
    console.log(floorYear)
    // need to add teYr to floorYear
    // get half (0 or 1)
    const teHalf = inputArr[1][0]
    // (teHalf * 19) + teHalfYr (0-18) = which year of the te it is (0-38)
    const teYr = (Number(teHalf * 19)) + Number(inputArr[1].slice(1,3))
    // add te year to the floor
    const year = floorYear + teYr

    console.log( year, teYr, teHalf)
    const dateTime = DateTime.fromObject({ year, ordinal: Number(inputArr[2]) }, {zone: "UTC"})

    return {
        dateTime,
        full: dateTime.toLocaleString({ ...DateTime.DATE_FULL, era: "short" })
    }
}

module.exports = {
    getKairosTime,
    convertKairoTimeArrToGregorian,
}